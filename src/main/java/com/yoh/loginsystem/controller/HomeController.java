package com.yoh.loginsystem.controller;

import org.springframework.security.core.context.SecurityContextHolder;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.yoh.loginsystem.model.User;

@Controller
public class HomeController {

	@GetMapping("/home")
	public ModelAndView index(){
		ModelAndView mav = new ModelAndView("index");
		User user =  (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		mav.addObject("username", user.getUsername());
		return mav;
	}
	
}
