package com.yoh.loginsystem.bootload;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.yoh.loginsystem.model.Permission;
import com.yoh.loginsystem.model.Role;
import com.yoh.loginsystem.model.User;
import com.yoh.loginsystem.service.PermissionService;
import com.yoh.loginsystem.service.RoleService;
import com.yoh.loginsystem.service.UserService;

@Component
public class Dataloader implements ApplicationListener<ContextRefreshedEvent>{

	
	private UserService userService;
	
	private RoleService roleService;
	
	private PermissionService permissionService;
	
	private PasswordEncoder passwordEncoder;
	
	

	@Override
	public void onApplicationEvent(ContextRefreshedEvent arg0) {
		System.out.println("Starting data loading");
		
		/*User admin_user =new User("admin", passwordEncoder.encode("123456789"), "Admin", "Admin lastname", "admin@gmail.com");
		User test_user =new User("test", passwordEncoder.encode("123456789"), "Test", "Test lastname", "test@gmail.com");
	
		Role role_admin = new Role("admin");
		Role role_user = new Role("user");
		
		Permission perm1 = new Permission("create_user");
		Permission perm2 = new Permission("update_user");
		Permission perm3 = new Permission("delete_user");
		Permission perm4 = new Permission("view_user");
		
		permissionService.addPermission(perm1);
		permissionService.addPermission(perm2);
		permissionService.addPermission(perm3);
		permissionService.addPermission(perm4);
		
		roleService.addRole(role_admin);
		roleService.addRole(role_user);
		
		admin_user.setEnabled(true);
		test_user.setEnabled(true);
		
		userService.addUser(admin_user);
		userService.addUser(test_user);
		
		
		roleService.assignPermissionToRole(perm1, role_admin);
		roleService.assignPermissionToRole(perm2, role_admin);
		roleService.assignPermissionToRole(perm3, role_admin);
		roleService.assignPermissionToRole(perm4, role_admin);
		roleService.assignPermissionToRole(perm1, role_user);
		roleService.assignPermissionToRole(perm2, role_user);
		
		userService.assignRoleToUser(role_admin, admin_user);
		userService.assignRoleToUser(role_user, admin_user);*/
	}

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@Autowired
	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}

	@Autowired
	public void setPermissionService(PermissionService permissionService) {
		this.permissionService = permissionService;
	}
	
	@Autowired
	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}
}
