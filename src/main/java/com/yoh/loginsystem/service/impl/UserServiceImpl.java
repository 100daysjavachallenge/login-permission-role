package com.yoh.loginsystem.service.impl;

import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.yoh.loginsystem.model.Role;
import com.yoh.loginsystem.model.User;
import com.yoh.loginsystem.repository.RoleRepository;
import com.yoh.loginsystem.repository.UserRepository;
import com.yoh.loginsystem.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService{

	static Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);
	
	private UserRepository userRepository;
	private RoleRepository roleRepository;
	//private EntityManager entityManager;
	
	
	/*@Autowired
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}*/
	
	@Autowired
	public void setRoleRepository(RoleRepository roleRepository) {
		this.roleRepository = roleRepository;
	}
	

	@Autowired
	public void setUserRepository(UserRepository userRepository){ //revisar despues
		this.userRepository = userRepository;
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		LOG.info("Get user by username = " +  username);
		
		User account = null;
		try {
			account = getUser(username);
		}catch(UsernameNotFoundException e1){
			e1.printStackTrace();
		}
		
		if(account == null){
			LOG.warn("NO SUCH USER!!!");
			throw new UsernameNotFoundException("No such user "+ username);
		}else if(account.getRoles().isEmpty()){
			LOG.warn("User has no authorities");
			throw new UsernameNotFoundException("User "+username+"has no authorities");
		}
		
		account.setLastAccessedDate(Calendar.getInstance().getTime());
		
		try{
			updateUser(account);
		}catch(UsernameNotFoundException e){
			e.printStackTrace();
		}
	
		return account;
	}


	@Override
	public User getUser(Long userId) {
		LOG.info("Get user by ID = " + userId);
		return userRepository.findOne(userId);
	}

	@Override
	public User getUser(String username) {
		LOG.info("Get user by username = " + username);
		return userRepository.findByUsername(username);
	}

	@Override
	public void updateUser(User user) {
		LOG.info("Update User = " + user.getUsername());
		User update_user = userRepository.getOne(user.getId());
		update_user.setUsername(user.getUsername());
		update_user.setFirstname(user.getFirstname());
		update_user.setLastname(user.getLastname());
		update_user.setPassword(user.getPassword());
		update_user.setRoles(user.getRoles());
		update_user.setLastAccessedDate(user.getLastAccessedDate());
		userRepository.save(update_user);
		
	}

	@Override
	public void deleteUser(Long userId) {
		
		LOG.info("Deleting user = " + getUser(userId).getUsername());
		
		User d_user = getUser(userId);
		
		if(d_user != null){
			userRepository.delete(d_user);
		}
	}

	@Override
	public List<User> getUsers() {
		
		return userRepository.findAll();
	}

	@Override
	@Transactional
	public void assignRoleToUser(Role role, User user) {
		Role assign_role = roleRepository.findByName(role.getName());
		if (assign_role == null){
			throw new NoResultException("Role does ot exist");
		}
		
		User assign_user = getUser(user.getId());
		if (assign_user == null){
			throw new NoResultException("User does ot exist");
		}
		
		assign_user.getRoles().add(assign_role);
		//entityManager.merge(assign_user);
		userRepository.save(assign_user);
		
	}

	@Override
	public void addUser(User user) {
		System.out.println("Admin PAr dios");
		
		userRepository.save(user);
	}

}
