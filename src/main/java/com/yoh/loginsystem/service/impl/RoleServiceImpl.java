package com.yoh.loginsystem.service.impl;

import java.util.List;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yoh.loginsystem.model.Permission;
import com.yoh.loginsystem.model.Role;
import com.yoh.loginsystem.model.User;
import com.yoh.loginsystem.repository.PermissionRepository;
import com.yoh.loginsystem.repository.RoleRepository;
import com.yoh.loginsystem.repository.UserRepository;
import com.yoh.loginsystem.service.RoleService;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    static Logger LOG = LoggerFactory.getLogger(RoleServiceImpl.class);
	
	private PermissionRepository permissionRepository;
	private RoleRepository roleRepository;
	
	@Autowired
	public void setPermissionRepository(PermissionRepository permissionRepository) {
		this.permissionRepository = permissionRepository;
	}

	@Autowired
	public void setRoleRepository(RoleRepository roleRepository) {
		this.roleRepository = roleRepository;
	}

	@Override
	public void addRole(Role role) {
		roleRepository.save(role);
	}

	@Override
	public Role getRole(Long id) {
		return roleRepository.getOne(id);
	}

	@Override
	public Role getRole(String rolename) {
		return roleRepository.findByName(rolename);
	}

	@Override
	public void updateRole(Role role) {
		Role update_role = roleRepository.findOne(role.getId());
		update_role.setName(role.getName());
		roleRepository.save(update_role);
	}

	@Override
	public void deleteRole(Long id) {
		// TODO Auto-generated method stub
		roleRepository.delete(id);
		
	}

	@Override
	public List<Role> getRoles() {
		return roleRepository.findAll();
	}

	@Override
	public void assignPermissionToRole(Permission permission, Role role) {
		LOG.info("Assign permission [" + permission.getName() + " ] to roler ->"+role.getName());
		Role assign_role = roleRepository.findByName(role.getName());
		if (assign_role == null){
			throw new NoResultException("Role does ot exist");
		}
		
		Permission assign_perm = permissionRepository.findByName(permission.getName());
		if (assign_perm == null){
			throw new NoResultException("Permission does ot exist");
		}
		
		assign_role.getPermission().add(assign_perm);
		roleRepository.save(assign_role);
		
	}

}
