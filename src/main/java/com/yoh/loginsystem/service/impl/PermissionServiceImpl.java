package com.yoh.loginsystem.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yoh.loginsystem.model.Permission;
import com.yoh.loginsystem.repository.PermissionRepository;
import com.yoh.loginsystem.repository.RoleRepository;
import com.yoh.loginsystem.service.PermissionService;

@Service
@Transactional
public class PermissionServiceImpl implements PermissionService {

	static Logger LOG = LoggerFactory.getLogger(PermissionServiceImpl.class);
		
	private PermissionRepository permissionRepository;
	
	
	@Autowired
	public void setPermissionRepository(PermissionRepository permissionRepository) {
		this.permissionRepository = permissionRepository;
	}

	@Override
	public void addPermission(Permission permission) {
		permissionRepository.save(permission);
		
	}

	@Override
	public Permission getPermission(Long id) {
		return permissionRepository.getOne(id);
	}

	@Override
	public Permission getPermission(String permissionname) {
		return permissionRepository.findByName(permissionname);
	}

	@Override
	public void updatePermission(Permission permission) {
		Permission update_perm = permissionRepository.findOne(permission.getId());
		update_perm.setName(permission.getAuthority());
		permissionRepository.save(update_perm);
		
	}

	@Override
	public void deletePermission(Long id) {
		permissionRepository.delete(id);
		
	}

	@Override
	public List<Permission> getPermissions() {
		// TODO Auto-generated method stub
		return permissionRepository.findAll();
	}

}
