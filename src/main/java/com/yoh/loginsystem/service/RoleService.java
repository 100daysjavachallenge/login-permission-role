package com.yoh.loginsystem.service;

import java.util.List;

import com.yoh.loginsystem.model.Permission;
import com.yoh.loginsystem.model.Role;

public interface RoleService {

	public void addRole(Role role);
	
	public Role getRole(Long id);
	
	public Role getRole(String rolename);
	
	public void updateRole(Role role);
	
	public void deleteRole(Long id);
	
	public List<Role> getRoles();
	
	public void assignPermissionToRole(Permission permission, Role role);
	
}
