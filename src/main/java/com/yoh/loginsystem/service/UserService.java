package com.yoh.loginsystem.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.yoh.loginsystem.model.Role;
import com.yoh.loginsystem.model.User;

public interface UserService extends UserDetailsService {

	public void addUser(User user);

	public User getUser(Long userId);

	public User getUser(String username);

	public void updateUser(User user);

	public void deleteUser(Long userId);

	public List<User> getUsers();

	public void assignRoleToUser(Role role, User user);

}
