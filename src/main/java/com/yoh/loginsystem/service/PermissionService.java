package com.yoh.loginsystem.service;

import java.util.List;

import com.yoh.loginsystem.model.Permission;

public interface PermissionService {

	public void addPermission(Permission permission);
	
	public Permission getPermission(Long id);
	
	public Permission getPermission(String permissionname);
	
	public void updatePermission(Permission permission);
	
	public void deletePermission(Long id);
	
	public List<Permission> getPermissions();
}
